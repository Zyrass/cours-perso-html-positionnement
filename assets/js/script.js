const btnRemove1 = document.getElementById("removeBTN1");

const listRectangle1 = document.querySelectorAll(".rectangle-1");

listRectangle1.forEach( data => {
  if (data.classList.contains("rectangle-1")) {
    btnRemove1.addEventListener("click", $e => {
      $e.preventDefault();
      data.classList.toggle("rectangle-1");
    })
  }
});