# Cours HTML - Le position relatif ou absolue

> Ici il est question de comprendre le principe du position relatif et du positionnement absolute.
> Via deux exemples, j'essai d'expliquer au mieux ce positionnement.

Ici il s'agit simplement d'un visuel codé en HTML / SCSS et une petite touche de JS dans le premier article afin de voir chaque nuance.

Il me sert essentiellement à expliquer la notion pour ceux qui auraient encore du mal avec celle-ci.